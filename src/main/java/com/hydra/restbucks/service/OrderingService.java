package com.hydra.restbucks.service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.hydra.restbucks.resource.Order;
import com.hydra.restbucks.resource.OrderItem;
import com.hydra.restbucks.resource.Product;

@Service
public class OrderingService {

//	private static final String SECRET = "ncBFh7PJ4StTvKZH";
	private Map<String, Order> orders = new HashMap<String, Order>();
	private static int nextOrderId;
	private static Order sampleOrder;
	
	public OrderingService(){
		initiateDemo();
	}
	
	private void initiateDemo(){
		Product product1 = new Product("Espresso");
		OrderItem oi = new OrderItem(product1, 1);
		Order order = new Order("1", oi);
		setOrderFields("1", order);
		
		sampleOrder = new Order(oi);
		
		orders.put("1", order);
		nextOrderId = 2;
	}
	
	private void setOrderFields(String id, Order order){
		order.setOrderNumber(id);
		order.setDiscountCode(UUID.randomUUID().toString());
	}
	
	public Order getOrder(String id){
		return orders.get(id);
	}
	
	public String addOrder(Order order){
		String id = Integer.toString(nextOrderId++);
		setOrderFields(id, order);
		orders.put(id, order);
		return id;
	}
	
	public Order removeOrder(String id){
		return orders.remove(id);
	}
	
	public boolean updateOrder(String id, Order order){
		
		if (orders.get(id) == null){
			return false;
		} else {
			setOrderFields(id, order);
			orders.put(id, order);
			return true;
		}		
	}
	
	public Order getSampleOrder(){
		return sampleOrder;
	}
}
