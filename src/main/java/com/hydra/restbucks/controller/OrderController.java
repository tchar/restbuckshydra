package com.hydra.restbucks.controller;

//import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
//import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import static de.escalon.hypermedia.spring.AffordanceBuilder.linkTo;
import static de.escalon.hypermedia.spring.AffordanceBuilder.methodOn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hydra.restbucks.exceptions.ResourceNotFoundException;
import com.hydra.restbucks.resource.Order;
import com.hydra.restbucks.service.OrderingService;

@Controller
@RequestMapping(value = "/api/order")
public class OrderController {

	private final static Logger LOGGER = LoggerFactory.getLogger(OrderController.class);
	
	@Autowired
	private OrderingService orderingService;
		
	@RequestMapping(value = "/", method = RequestMethod.POST, produces = "application/ld+json")
	@ResponseBody
	public ResponseEntity<Resource<Order>> createOrder(@RequestBody Order order){
		String id = orderingService.addOrder(order);
	
		Resource<Order> resource = new Resource<Order>(order);
		
		resource.add(linkTo(methodOn(OrderController.class).getOrder(id))
				.and(linkTo(methodOn(OrderController.class).updateOrder(id, order)))
				.and(linkTo(methodOn(OrderController.class).deleteOrder(id)))
				.withSelfRel());
		
		LOGGER.info("POST request on /api/order" + order.getOrderNumber());
		return new ResponseEntity<Resource<Order>>(resource, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/ld+json")
	@ResponseBody
	public ResponseEntity<Resource<Order>> getOrderNoId(){
	
		Order order = orderingService.getSampleOrder();
		
		Resource<Order> resource = new Resource<Order>(order);
		
		resource.add(linkTo(methodOn(OrderController.class).createOrder(order)).withSelfRel());
		
		LOGGER.info("GET request on /api/order");
		return new ResponseEntity<Resource<Order>>(resource, HttpStatus.METHOD_NOT_ALLOWED);
	}
		
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/ld+json")
	@ResponseBody
	public ResponseEntity<Resource<Order>> getOrder(@PathVariable String id){
		
		Order order = orderingService.getOrder(id);
		
		if (order == null){
			LOGGER.info("GET request on /api/order/" + id +" not found");
			throw new ResourceNotFoundException("Order not found");
		}
		
		Resource<Order> resource = new Resource<Order>(order);
		
		resource.add(linkTo(methodOn(OrderController.class).getOrder(id))
				.and(linkTo(methodOn(OrderController.class).updateOrder(id, order)))
				.and(linkTo(methodOn(OrderController.class).deleteOrder(id)))
				.withSelfRel());

		LOGGER.info("GET request on /api/order/" + order.getOrderNumber());
		return new ResponseEntity<Resource<Order>>(resource, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/ld+json")
	@ResponseBody
	public ResponseEntity<Resource<Order>> updateOrder(@PathVariable String id, @RequestBody Order order){
		//String id = orderingService.addOrder(order);
	
		if (!orderingService.updateOrder(id, order)){
			LOGGER.info("GET request on /api/order/" + id +" not found");
			throw new ResourceNotFoundException("Order not found");
		}
		
		Resource<Order> resource = new Resource<Order>(order);	
		
		resource.add(linkTo(methodOn(OrderController.class).getOrder(id))
				.and(linkTo(methodOn(OrderController.class).updateOrder(id, order)))
				.and(linkTo(methodOn(OrderController.class).deleteOrder(id)))
				.withSelfRel());
		
		LOGGER.info("PUT request on /api/order/" + order.getOrderNumber());
		return new ResponseEntity<Resource<Order>>(resource, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "application/ld+json")
	public ResponseEntity<Object> deleteOrder(@PathVariable String id){
		Order order = orderingService.removeOrder(id);
		
		if (order == null){
			LOGGER.info("GET request on /api/order/" + id +" not found");
			throw new ResourceNotFoundException("Order not found");
		}
		
		LOGGER.info("DELETE request on /api/order/" + order.getOrderNumber());
		return new ResponseEntity<Object>(HttpStatus.OK);
	}


}
