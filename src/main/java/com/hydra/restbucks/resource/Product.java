package com.hydra.restbucks.resource;

import de.escalon.hypermedia.action.Input;
import de.escalon.hypermedia.action.Type;

public class Product {

	private String category = "coffee";
	private String name;
	
	public Product(){
		
	}
	
	public Product(String name){
		this.setName(name);
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(@Input(value = Type.TEXT) String category) {
		this.category = category;
	}

	public String getName() {
		return name;
	}

	public void setName(@Input(value = Type.TEXT) String name) {
		this.name = name;
	}
	
}
