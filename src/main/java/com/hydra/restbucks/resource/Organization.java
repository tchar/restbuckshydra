package com.hydra.restbucks.resource;

import de.escalon.hypermedia.action.Input;
import de.escalon.hypermedia.action.Type;

public class Organization {

	private String legalName;
	
	public Organization(){
		
	}
	
	public Organization(String name) {
		this.setLegalName(name);
	}

	public String getLegalName() {
		return legalName;
	}

	public void setLegalName(@Input(value = Type.TEXT) String legalName) {
		this.legalName = legalName;
	}

}
