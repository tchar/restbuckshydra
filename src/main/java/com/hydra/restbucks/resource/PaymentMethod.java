package com.hydra.restbucks.resource;

import de.escalon.hypermedia.hydra.mapping.Expose;

public enum PaymentMethod {
	@Expose("gr:Cash")
	CASH,
	@Expose("gr:ByBankTransferInAdvance")
	BYBANKTRANSFERINADVANCE,
	@Expose("gr:ByInvoice")
	BYINVOICE,
	@Expose("gr:CheckInAdvance")
	CHECKINADVANCE,
	@Expose("gr:COD")
	COD,
	@Expose("gr:DirectDebit")
	DIRECTDEBIT,
	@Expose("gr:GoogleCheckout")
	GOOGLECHECKOUT,
	@Expose("gr:PayPal")
	PAYPAL,
	@Expose("gr:PaySwarm")
	PAYSWARM
}
