package com.hydra.restbucks.resource;

import java.util.Arrays;
import java.util.LinkedList;

import de.escalon.hypermedia.action.Input;
import de.escalon.hypermedia.action.Type;
import de.escalon.hypermedia.hydra.mapping.Term;
import de.escalon.hypermedia.hydra.mapping.Terms;
import de.escalon.hypermedia.hydra.mapping.Vocab;

@Vocab
@Terms({
	@Term(define = "gr", as = "http://purl.org/goodrelations/v1#"),
	@Term(define = "hydra", as = "http://www.w3.org/ns/hydra/core#")})
public class Order {
	
	private Organization seller = new Organization("Restbucks");
	private double discount = 0;
	private String discountCode = null;
	private PaymentMethod paymentMethod = PaymentMethod.CASH;
	private LinkedList<OrderItem> orderedItem = new LinkedList<OrderItem>();
	private String orderNumber = null;

	
	public Order(){
	}
	
	
	public Order(String id, OrderItem...items){
		this.orderNumber = id;
		this.orderedItem = new LinkedList<OrderItem>(Arrays.asList(items));
		
		//defaults
		this.discount = 0;
		this.discountCode = null;
		this.paymentMethod = PaymentMethod.CASH;
	}
	
	
	public Order(OrderItem... items) {
		this.orderedItem = new LinkedList<OrderItem>(Arrays.asList(items));
		this.discountCode = "1234-5678-9012-456";
		this.paymentMethod = PaymentMethod.CASH;
	}


	public Organization getSeller() {
		return seller;
	}
	
	public void setSeller(@Input(value = Type.FROM_JAVA) Organization seller){
		this.seller = seller;
	}


	public double getDiscount() {
		return discount;
	}


	public void setDiscount(@Input(value = Type.NUMBER, min = 0)double discount) {
		this.discount = discount;
	}


	public String getDiscountCode() {
		return discountCode;
	}


	public void setDiscountCode(@Input(value = Type.TEXT) String discountCode) {
		this.discountCode = discountCode;
	}


	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}


	public void setPaymentMethod(@Input(value = Type.FROM_JAVA) PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}


	public LinkedList<OrderItem> getOrderedItem() {
		return orderedItem;
	}


	public void setOrderedItem(@Input(value = Type.FROM_JAVA) LinkedList<OrderItem> orderedItem) {
		this.orderedItem = orderedItem;
	}


	public String getOrderNumber() {
		return orderNumber;
	}


	public void setOrderNumber(@Input(value = Type.TEXT) String orderNumber) {
		this.orderNumber = orderNumber;
	}
	
}
