package com.hydra.restbucks.resource;

import de.escalon.hypermedia.action.Input;
import de.escalon.hypermedia.action.Type;

public class OrderItem {

	private Product orderedItem;
	private int orderQuantity = 1;
	
	public OrderItem(){
		
	}
	
	public OrderItem(Product product, int quantity){
		this.orderedItem = product;
		this.orderQuantity = quantity;
	}
	
	
	public int getOrderQuantity() {
		return orderQuantity;
	}

	public void setOrderQuantity(@Input(value = Type.NUMBER, min = 0) int orderQuantity) {
		this.orderQuantity = orderQuantity;
	}

	public Product getOrderedItem() {
		return orderedItem;
	}

	public void setOrderedItem(@Input(value = Type.FROM_JAVA) Product orderedItem) {
		this.orderedItem = orderedItem;
	}
}
